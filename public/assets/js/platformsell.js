$(document).ready(function() {
    $('#coin-input').on('input', function() {
        var amt = $(this).val();
        var coinValue = parseFloat($('#coin-current').text());
        var result = Number($(this).val() * coinValue);
        var purchasedCoin = parseFloat($('#coin-price').text());
        if(coinValue >= purchasedCoin) {
            $('#coin-cost').css('color', 'green');
            $('#total-profit').css('color', 'green');
        } else {
            $('#coin-cost').css('color', 'red');
            $('#total-profit').css('color', 'red');
        }
        $('#coin-cost').html("$" + result.toFixed(2));
        var profit = result - (purchasedCoin * Number($(this).val()));
        $('#total-profit').html("Return: $" + profit.toFixed(2));
        if(amt == 0) {
            $('.coin-amt').text("0");
        } else {
            $('.coin-amt').text(amt);
        }
    });
});